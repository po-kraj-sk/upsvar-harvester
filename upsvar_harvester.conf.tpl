# configuration for harvester in Docker image
[upsvar_harvester]
db = postgresql+psycopg://$DB_CONNECTION_STRING
db_schema = $DB_SCHEMA

index_columns=$INDEX_COLUMNS
kod_nuts_filter=$LOCATIONS_REGEX_FILTER

be_careful = $BE_CAREFUL
# 1 day, in seconds
#cache_expire = 86400
#throttle_sleep=0.25

[requests]
retry_connect=$RETRY_CONNECT
retry_backoff=$RETRY_BACKOFF

[sqlalchemy]
#echo = False

[prometheus]
uri = %(PROM_PUSHGATEWAY_URI)s
