# Intro

In order to be nice to UPSVAR, i.e. use data from them responsibly, this
script harvests selected data cubes into local PostgreSQL database.

On first run it fetches all the data, on subsequent runs only updates. Thus
it is supposed to be run periodically, from the `cron`, once a week or at
most once a day (since source data is usually updated monthly), preferably
during the night.

What does it mean "responsibly": While harvesting does make some load on the
server(s) of UPSVAR, when performing more complicated analytics or
programming apps, it is best to have local copy of the data to avoid
repeated requests for same data. Otherwise load from analytics or apps on
UPSVAR may get unbearable and cause disruption of their service.


## About UPSVAR data

We are using "preview version" of new Open Data publication methodology of
UPSVAR, see:

https://www.upsvr.gov.sk/statistiky/open-data.html?page_id=955243


"Preview" can be understood as "beta" and during this period, UPSVAR is
collecting feedback regarding how to publish data better.


# License

This code is licensed under the [EUPL](LICENSE.txt).


# Requirements

Python 3.11 or newer


# How to install

1. checkout from Git: https://gitlab.com/po-kraj-sk/upsvar-harvester.git
2. create virtual environment: `virtualenv --python=python3.11 .venv`
3. activate: `source .venv/bin/activate`
4. install dependencies: `pip install -r requirements.txt`
5. create configuration file: `cp upsvar_harvester.conf.example upsvar_harvester.conf`

Then also check:

6. that database exists along with schema 'laboroffice'
7. user running harvester is able to create tables in the database schema
8. configuration in `upsvar_harvester.conf`, adjust if necessary


# How to run

`python upsvar_harvester.py`


## be_careful setting

When `True`, harvester will try avoiding overloading either source server
or target database.

Following will be done to avoid overloading source server:

1. server responses will be kept in local SQlite cache
2. after getting data from server, small sleep will be done


## Updates of dataset schema

As of now, code does not handle changes in dataset structure (i.e. DB
schema updates).

If the structure changes, DB (or affected tables) needs to be dropped and
all data downloaded again.

Or DB schema (table names and structure) needs to be updated by hand.


# Using Docker
A Dockerfile and a docker-compose are available. The docker-compose provides an
easy way to build and test the docker image and the harvesting service.

To see the harvester in action:
    # build the image
    docker-compose build
    # run it
    docker-compose up -d
    # follow the logs
    docker-compose logs -f

You can see the results in the associated database, on port 5433 (use your
favorite tool for database inspection).

The docker-compose.yml file shows you how to configure the docker container
using environment variables. The list of available environment variables can be
seen in the Dockerfile.

*One specific case to remark: LOCATIONS_REGEX_FILTER has an `$` character in it.
 This is considered by docker-compose as a variable substitution symbol, so it
 had to be escaped by doubling it, hence the `$$` at the end of it.*

## Logs persistence
You are expected to bind a volume to `/app/logs` folder. If bind-mounting to a FS folder, the chown command from the Dockerfile will not  operate: you will have to manually create and chown the volume folder to uid 999


# Information for developers

Branches, pull-requests, releases, etc.: according to [git-flow](http://danielkummer.github.io/git-flow-cheatsheet/) but ...

... in order to allow better overview for current and future contributors, we'd like to conclude features with pull-requests,
i.e. instead of `git flow feature finish MYFEATURE` do following:

1. `git flow feature publish MYFEATURE`
2. go to GitLab and open pull-request from your feature branch to `develop`
3. review + adjustments
4. merge + delete branch after merge


## Git hooks

For source code formatting, etc.:

`pre-commit install`
