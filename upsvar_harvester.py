#!/usr/bin/python3
#
# Copyright 2020 by Peter Hanecak <hanecak+wb@hany.sk>
#
# Licensed under the EUPL, Version 1.2 or - as soon they will be approved
# by the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at:
#
# https://joinup.ec.europa.eu/software/page/eupl 5
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing permissions
# and limitations under the Licence.
#
#
# This harvests and keeps up-to-date statistical data from UPSVAR
# (https://www.upsvr.gov.sk/) in local PostgreeSQL database.
#
# Main points:
# a) on first run creates tables in empty DB and gets all data from UPSVAR
# b) on subsequent runs gets only updates
# c) one dataset, all periods -> one DB table

import calendar
import configparser
import datetime
import logging
import os
import re
import sys
import time
from argparse import ArgumentParser
from logging.config import fileConfig

import requests
import requests_cache
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry

from sqlalchemy import create_engine, inspect
from sqlalchemy import Column, ForeignKey, MetaData, Table, UniqueConstraint
from sqlalchemy import Date, Float, Integer, String
from sqlalchemy.dialects.postgresql import insert
from sqlalchemy.exc import NoResultFound
from sqlalchemy.orm import declarative_base, registry, relationship, sessionmaker
from sqlalchemy.sql import text

from prometheus_client import CollectorRegistry, Gauge, Summary, push_to_gateway


fileConfig("logging_config.ini")
logger = logging.getLogger()


class UHConfig:

    CONFIG_FILE_NAME = "upsvar_harvester.conf"
    CONFIG_SECTION_HARVESTER = "upsvar_harvester"
    CONFIG_SECTION_PROMETHEUS = "prometheus"
    CONFIG_SECTION_REQUESTS = "requests"
    CONFIG_SECTION_SQLALCHEMY = "sqlalchemy"

    def __init__(self):
        # monkey-patch -> ugly, but env. variables are case insensitive and thus with default
        # behavior of ConfigParser may trigger duplicates exception
        configparser.ConfigParser.optionxform = str
        cfg_parser = configparser.ConfigParser(os.environ)
        cfg_parser.read(self.CONFIG_FILE_NAME)

        try:
            # harvester - mandatory:
            self.db_string = cfg_parser.get(self.CONFIG_SECTION_HARVESTER, "db")

            # harvester - optional:
            self.be_careful = cfg_parser.getboolean(
                self.CONFIG_SECTION_HARVESTER, "be_careful", fallback=True
            )
            self.cache_expire = cfg_parser.getint(
                self.CONFIG_SECTION_HARVESTER, "cache_expire", fallback=86400
            )
            self.db_schema = cfg_parser.get(
                self.CONFIG_SECTION_HARVESTER, "db_schema", fallback="laboroffice"
            )
            self.index_columns = cfg_parser.get(
                self.CONFIG_SECTION_HARVESTER, "index_columns", fallback=None
            )
            self.kod_nuts_filter = cfg_parser.get(
                self.CONFIG_SECTION_HARVESTER, "kod_nuts_filter", fallback=None
            )
            self.throttle_sleep = cfg_parser.getfloat(
                self.CONFIG_SECTION_HARVESTER, "throttle_sleep", fallback=0.25
            )

            # Monitoring config (optional).If not defined, it won't send metrics
            self.prometheus_uri = cfg_parser.get(
                self.CONFIG_SECTION_PROMETHEUS, "uri", fallback=None
            )

            # requests library - optional:
            self.retry_connect = cfg_parser.getint(
                self.CONFIG_SECTION_REQUESTS, "retry_connect", fallback=3
            )
            self.retry_backoff = cfg_parser.getfloat(
                self.CONFIG_SECTION_REQUESTS, "retry_backoff", fallback=0.25
            )

            # SQLAlchemy - optional:
            self.sqlalchemy_echo = cfg_parser.getboolean(
                self.CONFIG_SECTION_SQLALCHEMY, "echo", fallback=False
            )
        except (configparser.NoOptionError, configparser.NoSectionError) as e:
            logger.error("%s (file %s)" % (e, self.CONFIG_FILE_NAME))
            sys.exit(-1)


config = UHConfig()

# Prometheus metrics
prom_registry = CollectorRegistry()

prom_harvest_time = Summary(
    "request_processing_seconds",
    "Time spent processing request",
    registry=prom_registry,
)
prom_updated_dataset = Gauge(
    "harvester_updated_dataset",
    "Number of datasets updated during the harvest",
    registry=prom_registry,
)
prom_last_harvest_time = Gauge(
    "harvester_last_run",
    "Last time the harvest was run (finished) in ms",
    registry=prom_registry,
)


Base = declarative_base(metadata=MetaData(schema=config.db_schema))


class DatasetMetadata(Base):
    __tablename__ = "metadata_dataset"

    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False, unique=True)
    title = Column(String)
    description = Column(String)

    periods = relationship("PeriodMetadata")

    def __repr__(self):
        return "<Dataset(id='%s', name='%s', last_update='%s', ...)>" % (
            self.id,
            self.name,
            self.last_update,
        )


class PeriodMetadata(Base):
    __tablename__ = "metadata_period"

    id = Column(Integer, primary_key=True)
    dataset_id = Column(Integer, ForeignKey("metadata_dataset.id"))
    period = Column(Date, nullable=False)
    etag = Column(String)  # 'ETag' from HTTP response
    last_update = Column(Date)  # 'pubDate' in UPSVAR dataset

    dataset = relationship("DatasetMetadata", back_populates="periods")

    __table_args__ = (
        UniqueConstraint("dataset_id", "period", name="metadata_period_uc"),
    )

    def __repr__(self):
        return "<Period(id='%s', dataset='%s', period='%s', ...)>" % (
            self.id,
            self.dataset.name,
            self.period,
        )


class VersionMetadata(Base):
    __tablename__ = "metadata_version"

    id = Column(Integer, primary_key=True)
    key = Column(String, nullable=False, unique=True)
    value = Column(Integer)

    def __repr__(self):
        return "<Version(id='%s', key='%s', value='%s', ...)>" % (
            self.id,
            self.key,
            self.value,
        )


# see https://requests-cache.readthedocs.io/en/latest/user_guide.html#usage
def make_throttle_hook(timeout=1.0):
    def hook(response, *args, **kwargs):
        if not getattr(response, "from_cache", False):
            logger.debug("throttle hook: timeout: %s" % timeout)
            time.sleep(timeout)
        return response

    return hook


class UpsvarHarvester:
    """
    Regarding "data and metadata model" in this harvester:

    On UPSVAR page, we have periods (2020-01, 2020-01, etc.) and under them we have "tables" (UoZ-01-zakladne-ukazovatele, etc.).
    We can say that each file is a dataset and thus we have `periods x tables` datasets (amount of). That's fine for publication.

    But we're going to twist that here, in harvester: Since we do not want `periods * tables` tables in our DB, we're going
    to merge datasets of all periods for same "table" into one DB table. Thus here, dataset consists of several periods.
    Hence structure described by `DatasetMetadata` and `PeriodMetadata`.
    """

    DB_VERSION_KEY = "db"
    DB_VERSION = 1

    URL_BASE = "https://www.upsvr.gov.sk"
    URL_HOME_PAGE = URL_BASE + "/statistiky/open-data/%Y-%m.html"

    UPSVAR_DEFAULT_TIME_BACKLOG = datetime.date(2023, 1, 1)
    UPSVAR_PUB_DELAY = datetime.timedelta(
        days=48
    )  # 20th of the next month, approximated

    UPSVAR_JSON_NUTS_CODE = "kod_nuts"

    DB_COLUMN_PERIOD = "period"

    COLUMNS_STR = ["kod_nuts", "uzemie"]
    COLUMNS_FLOAT = [
        "miera_evidovanej_nezam",
        "miera_nezamestnanosti",
        "podiel_dispuoz_vpv_naopv",
        "podiel_uoz_v_pv_na_opv",
    ]
    COLUMNS_DATE = [DB_COLUMN_PERIOD]

    def __init__(self):
        self.config = config

        requests_log = logging.getLogger("urllib3")
        requests_log.propagate = True

        self.kod_nuts_filter_re = None
        if config.kod_nuts_filter:
            self.kod_nuts_filter_re = re.compile(config.kod_nuts_filter)
        self.index_columns = []
        if config.index_columns:
            self.index_columns = config.index_columns.split(",")

        # HTTP requests stuff:
        self.requests_session = requests.Session()
        if config.be_careful:
            # to avoid hitting UPSVAR server too much,
            # we're going to cache all responses for configured amount of time
            self.requests_session = requests_cache.CachedSession(
                "upsvar_cache", expire_after=config.cache_expire
            )
            self.requests_session.hooks = {
                "response": make_throttle_hook(config.throttle_sleep)
            }
        retry = Retry(connect=config.retry_connect, backoff_factor=config.retry_backoff)
        adapter = HTTPAdapter(max_retries=retry)
        self.requests_session.mount("https://", adapter)

        # SQLAlchemy stuff:
        self.engine = create_engine(config.db_string, echo=config.sqlalchemy_echo)
        self.Session = sessionmaker(self.engine)
        self.session = self.Session()

        self.inspect = inspect(self.engine)
        self.mapper = registry()

        Base.metadata.create_all(self.engine, checkfirst=True)
        self.metadata = Base.metadata
        self.metadata.reflect(self.engine, schema=config.db_schema)
        logger.debug("DB: %s; schema: %s" % (config.db_string, config.db_schema))

        # statistics counters:
        self.counter_datasets = 0
        self.counter_datasets_total = 0

    @staticmethod
    def period_str2date(period):
        return datetime.datetime.strptime(period, "%Y-%m").date()

    @staticmethod
    def period_str2tuple(period):
        period_as_date = UpsvarHarvester.period_str2date(period)
        return (period_as_date.year, period_as_date.month)

    @staticmethod
    def period_tuple2date(year, month):
        return datetime.date(year, month, 1)

    @staticmethod
    def period_tuple2str(year, month):
        period_as_date = UpsvarHarvester.period_tuple2date(year, month)
        return period_as_date.strftime("%Y-%m")

    def _get_dataset_metadata(self, name):
        """Get our own metadata (our own = what we have in DB) about given dataset."""

        dm = None
        try:
            dm = (
                self.session.query(DatasetMetadata)
                .filter(DatasetMetadata.name == name)
                .one()
            )
        except NoResultFound:
            dm = DatasetMetadata(name=name)
            self.session.add(dm)

        return dm

    def _get_period_metadata(self, dm, period):
        """Get our own metadata (our own = what we have in DB) about given period of a dataset."""

        pm = None
        try:
            pm = (
                self.session.query(PeriodMetadata)
                .filter(
                    PeriodMetadata.dataset_id == dm.id, PeriodMetadata.period == period
                )
                .one()
            )
        except NoResultFound:
            pm = PeriodMetadata(dataset_id=dm.id, period=period)
            self.session.add(pm)

        return pm

    def _is_dataset_period_fresh(self, url, local_etag):
        """
        To avoid fetching whole dataset period file, we're going to do 'HEAD' request and look for Etag.
        """

        if not local_etag:
            # no ETag -> no local copy -> is not fresh
            return False

        logger.info("fetching dataset update info: URL: %s" % url)
        r = self.requests_session.head(url)

        return r.headers["ETag"] == local_etag

    @staticmethod
    def _derive_table_name(name):
        return name.replace("-", "_").lower()

    @staticmethod
    def _derive_class_name(name):
        return "Dataset" + name[0].upper() + name[1:].strip("-").lower()

    @staticmethod
    def _is_string_column(name):
        return name in UpsvarHarvester.COLUMNS_STR

    @staticmethod
    def _is_float_column(name):
        return name in UpsvarHarvester.COLUMNS_FLOAT

    @staticmethod
    def _is_date_column(name):
        return name in UpsvarHarvester.COLUMNS_DATE

    def _create_cube_table_column_obj(self, column_name):
        """
        Create an SQLAlchemy Column object.

        Main point here is to use proper type for given column.
        """

        index = column_name in self.index_columns

        if self._is_string_column(column_name):
            return Column(column_name, String, index=index)
        elif self._is_float_column(column_name):
            return Column(column_name, Float, index=index)
        elif self._is_date_column(column_name):
            return Column(column_name, Date, index=index)
        else:
            return Column(column_name, Integer, index=index)

    def _create_dataset_table(self, dm, pm, data):
        """Check whether given dataset's table already exist, create if not."""

        table_name = self._derive_table_name(dm.name)
        if self.inspect.has_table(table_name, schema=self.config.db_schema):
            return self.metadata.tables[self.config.db_schema + "." + table_name]

        dataset_table = Table(
            table_name, self.metadata, Column("id", Integer, primary_key=True)
        )

        uc_name_list = []

        column = self._create_cube_table_column_obj(self.DB_COLUMN_PERIOD)
        dataset_table.append_column(column)
        uc_name_list.append(column.name)

        for column_name in data["posts"]["data"][0].keys():
            column = self._create_cube_table_column_obj(column_name)
            dataset_table.append_column(column)
            if column_name == self.UPSVAR_JSON_NUTS_CODE:
                uc_name_list.append(column.name)

        uc_name = dataset_table.name + "_uc"
        unique_constraint = UniqueConstraint(*uc_name_list, name=uc_name)
        dataset_table.append_constraint(unique_constraint)

        # note: Maybe we can construct it even sooner (above) and while in the loop
        # creating Column objects we can also add fields to this class.
        data_class_name = self._derive_class_name(dm.name)
        data_class = type(data_class_name, (object,), {})

        self.mapper.map_imperatively(data_class, dataset_table)
        self.metadata.create_all(self.engine)
        logger.info("table %s created" % table_name)

        return dataset_table

    @staticmethod
    def __convert_float(str_value):
        if not str_value:
            return None
        return float(str_value)

    @staticmethod
    def __convert_int(str_value):
        if not str_value:
            return None
        return int(str_value)

    def etl_data(self, dm, pm, data):
        """High-level method which:
        1) makes sure we have DB table for given dataset
        2) gets all the data from the dataset and push it into our DB
        """

        actual_data = data["posts"]["data"]
        if len(actual_data) <= 0:
            logger.info("empty data: %s/%s" % (dm.name, pm.period))
            return

        table = self._create_dataset_table(dm, pm, data)

        column_names = actual_data[0].keys()
        counter_in = counter_out = 0
        try:
            for row in actual_data:
                counter_in += 1
                # filtering
                if (self.UPSVAR_JSON_NUTS_CODE in row) and self.kod_nuts_filter_re:
                    kod_nuts = row[self.UPSVAR_JSON_NUTS_CODE]
                    if self.kod_nuts_filter_re.search(kod_nuts) is None:
                        continue
                # extract + convert type
                data_obj = data_obj_upsert = {}
                data_obj[self.DB_COLUMN_PERIOD] = pm.period
                for column_name in column_names:
                    if self._is_string_column(column_name):
                        data_obj[column_name] = row[column_name]
                    elif self._is_float_column(column_name):
                        data_obj[column_name] = self.__convert_float(row[column_name])
                    else:
                        data_obj[column_name] = self.__convert_int(row[column_name])
                    # In case of upsert, we need all columns except those used as keys,
                    # i.e. 'period' is already out (see before 'for') and we need here
                    # to skip only 'kod_nuts':
                    if column_name != self.UPSVAR_JSON_NUTS_CODE:
                        data_obj_upsert[column_name] = data_obj[column_name]

                insert_stmt = insert(table).values(data_obj)
                do_update_stmt = insert_stmt.on_conflict_do_update(
                    constraint=table.name + "_uc", set_=data_obj_upsert
                )
                self.session.execute(do_update_stmt)
                counter_out += 1
        finally:
            self.session.commit()

        logger.debug(
            "%d (out of %d) rows added for %s/%s"
            % (counter_out, counter_in, dm.name, pm.period)
        )

    def process_dataset(self, year, month, name, url_path):
        """Checks whether given dataset+period needs fetching/update and if so, do it."""

        logger.info("processing dataset: %s" % url_path)
        dm = self._get_dataset_metadata(name)
        period_date = UpsvarHarvester.period_tuple2date(year, month)
        pm = self._get_period_metadata(dm, period_date)
        url = self.URL_BASE + url_path
        if self._is_dataset_period_fresh(url, pm.etag):
            logger.info("dataset %s is fresh, skipping update" % url_path)
            # TODO: check whether truly needed
            self.session.rollback()
            return

        try:
            # fetch data
            logger.info("fetching dataset: URL: %s" % url)
            r = self.requests_session.get(url)
            data = r.json()

            # get the data
            self.etl_data(dm, pm, data)

            # all done, make a note into status info
            pm.etag = r.headers["ETag"]
            description_data = data["posts"]["description"]
            pm.last_update = description_data["pubDate"]
            dm.title = description_data["title"]
            dm.description = description_data["description"]

            self.counter_datasets += 1
        except Exception as ex:
            logger.warning("error occurred, rolling back transaction for %s" % url_path)
            self.session.rollback()
            raise ex
        finally:
            self.session.commit()

    def get_dataset_list(self, year, month):
        """
        Get a dataset list from UPSVAR home page, we will harvest all known datasets found there.
        :return: list of '(name, url)' tuples,
                 ex. [('VPM-03-vzdelanie', '/statistiky/open-data/2020-01/VPM-03-vzdelanie.json')]
        """

        period_date = self.period_tuple2date(year, month)
        url = period_date.strftime(self.URL_HOME_PAGE)
        logger.info("fetching dataset list: url: %s" % url)
        r = self.requests_session.get(url)
        if r.status_code == 404:
            logger.debug("period %s not available yet" % period_date)
            return []
        period_homepage = r.text

        # very primitive HTML parsing
        DATASET_PATTERN = r'<a href="(/statistiky/open-data/[0-9]{4}-[01][0-9]/\S+\.json)">(\S+).json</a>'
        dataset_list = []
        for result in re.finditer(DATASET_PATTERN, period_homepage):
            dataset_list.append((result.group(2), result.group(1)))

        logger.debug("dataset list: %s" % dataset_list)
        return dataset_list

    def process_period(self, year, month):
        """
        Process all datasets for given period.
        :param year: year of the period (eg. 2020)
        :param month: month of the period (eg. 1)
        """
        # list of cubes we're going to harvest
        dataset_list = self.get_dataset_list(year, month)

        self.counter_datasets = 0
        for (dataset_name, dataset_url) in dataset_list:
            self.process_dataset(year, month, dataset_name, dataset_url)
        logger.info(
            "%d datasets updated for %s-%s" % (self.counter_datasets, year, month)
        )

        self.counter_datasets_total += self.counter_datasets

    def __parse_cmdln_periods(self, periods):
        period_list = []
        for period in periods:
            result = re.split(r"([0-9]{4})-([01][0-9])", period, 2)
            if (
                len(result) != 4
                or result[0]
                or result[3]
                or not result[1]
                or not result[2]
            ):
                raise ValueError("invalid period specification: %s" % period)
            period_list.append((int(result[1]), int(result[2])))
        return period_list

    def _parse_cmdln_options(self):
        period_list = None

        parser = ArgumentParser()
        parser.add_argument(
            "periods", metavar="period", nargs="*", help="a period to harvest"
        )
        parser.add_argument(
            "-v",
            "--verbose",
            action="store_true",
            dest="verbose",
            default=False,
            help="be more verbose",
        )
        args = parser.parse_args()

        if args.verbose:
            logging.getLogger().setLevel("DEBUG")
        if len(args.periods) > 0:
            period_list = self.__parse_cmdln_periods(args.periods)

        return period_list

    def _determine_harvest_periods(self):
        """
        Determine what we have vs. what is expected to be available and return list of periods
        for harvest (i.e. those assumed missing in out DB).
        """
        period = self.UPSVAR_DEFAULT_TIME_BACKLOG
        r = (
            self.session.query(PeriodMetadata.period)
            .order_by(PeriodMetadata.period.desc())
            .first()
        )
        if r:
            period = r.period

        now = datetime.datetime.now().date()
        # ... but UPSVAR is not publishing "real time", i.e. they need some time for processing:
        now -= self.UPSVAR_PUB_DELAY

        periods = []
        while period <= now:
            periods.append((period.year, period.month))

            days_in_month = calendar.monthrange(period.year, period.month)[1]
            delta = datetime.timedelta(days=days_in_month)
            period += delta

        return periods

    def _db_is_brand_new(self):
        # at first run, at this stage, we have empty 'metadata_*' tables
        dm_count = self.session.query(DatasetMetadata).count()
        return dm_count == 0

    def _get_db_version_metadata(self):
        try:
            vm = (
                self.session.query(VersionMetadata)
                .filter(VersionMetadata.key == self.DB_VERSION_KEY)
                .one()
            )
        except NoResultFound:
            version = self.DB_VERSION if self._db_is_brand_new() else 0
            vm = VersionMetadata(key=self.DB_VERSION_KEY, value=version)
            self.session.add(vm)
            logger.info("database version set to %d" % version)

        return vm

    def _migrate0to1(self, vm):
        statement = text(
            """
        ALTER TABLE %s.uoz_01_zakladne_ukazovatele
        ADD COLUMN obyvatelstvo_v_pv_spolu integer,
        ADD COLUMN pocet_disp_uoz_v_pv integer,
        ADD COLUMN pocet_nedisp_uoz_spolu integer,
        ADD COLUMN podiel_dispuoz_vpv_naopv double precision,
        ADD COLUMN podiel_uoz_v_pv_na_opv double precision;
        """
            % self.config.db_schema
        )
        self.session.execute(statement)

        statement = text(
            """
        ALTER TABLE %s.uoz_zeny_01_zakladne_ukazovatele
        ADD COLUMN obyvatelstvo_v_pv_spolu integer,
        ADD COLUMN pocet_disp_uoz_v_pv integer,
        ADD COLUMN pocet_nedisp_uoz_spolu integer,
        ADD COLUMN podiel_dispuoz_vpv_naopv double precision,
        ADD COLUMN podiel_uoz_v_pv_na_opv double precision;
        """
            % self.config.db_schema
        )
        self.session.execute(statement)

        statement = text(
            """
        ALTER TABLE %s.uoz_07_znevyh
        ADD COLUMN znev_p8i integer;
        """
            % self.config.db_schema
        )
        self.session.execute(statement)

        statement = text(
            """
        ALTER TABLE %s.uoz_zeny_07_znevyh
        ADD COLUMN znev_p8i integer;
        """
            % self.config.db_schema
        )
        self.session.execute(statement)

        vm.value = 1
        self.session.commit()

        logger.info("database version update: 0 -> 1")

    def update_db_schema(self):
        vm = self._get_db_version_metadata()
        version = vm.value
        logger.debug("database version: %d" % version)
        if version == self.DB_VERSION:
            return

        if version <= 0:
            self._migrate0to1(vm)

        # reload table metadata
        self.metadata.clear()
        self.metadata.reflect(self.engine, schema=config.db_schema)

    @prom_harvest_time.time()
    def main(self):
        # DB schema updates, if needed
        self.update_db_schema()

        # list of periods we're going to harvest
        period_list = self._parse_cmdln_options()
        if not period_list:
            period_list = self._determine_harvest_periods()

        for (year, month) in period_list:
            self.process_period(year, month)
        logger.info("%d datasets updated" % self.counter_datasets_total)

        # Send metrics to prometheus pushgateway
        if self.config.prometheus_uri:
            try:
                prom_updated_dataset.set(self.counter_datasets_total)
                prom_last_harvest_time.set(time.time() * 1000)
                logger.info("Pushing metrics to prometheus")
                push_to_gateway(
                    self.config.prometheus_uri,
                    job="upsvar-harvester",
                    registry=prom_registry,
                )
                # push_to_gateway(self.config.prometheus_uri, job='health-harvester', registry=prom_registry)
            except Exception:
                logger.error(
                    "Unable to reach prometheus pushgateway {}".format(
                        self.config.prometheus_uri
                    )
                )


if __name__ == "__main__":
    upsvar_harvester = UpsvarHarvester()
    upsvar_harvester.main()
