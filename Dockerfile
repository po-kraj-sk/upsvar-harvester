FROM python:3.11

LABEL project="georchestra.org"
LABEL org.opencontainers.image.authors="jeanpommier@pi-geosolutions.fr"

#install envsubst
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
        gettext-base \
        ncat \
    && rm -rf /var/lib/apt/lists/*


# Add non-privileged user to run the script
RUN useradd -M -s /bin/bash -U harvester -u 999

# Copy the application
COPY --chown=harvester:harvester requirements.txt logging_config* upsvar_harvester* LICENSE.txt README.md /app/
RUN pip install -r /app/requirements.txt
WORKDIR /app

# Set up entrypoint. Will take care of the app's configuration
COPY --chown=harvester:harvester docker /
RUN chmod +x /entrypoint.sh &&\
    chmod +x /docker-entrypoint.d/*
ENTRYPOINT [ "/entrypoint.sh" ]

ENV LOCATIONS_REGEX_FILTER="^(SK041.+|SK0|SK0[0-9]{2})$" \
    INDEX_COLUMNS="period,kod_nuts" \
    DB_CONNECTION_STRING="" \
    DB_CONNECTION_STRING_FILE="" \
    DB_SCHEMA="laboroffice" \
    LOG_LEVEL="INFO" \
    BE_CAREFUL="True" \
    RETRY_CONNECT=3 \
    RETRY_BACKOFF=0.25 \
    LOG_FILE="/app/logs/harvesters/upsvar_harvester.log"

RUN mkdir -p /app/logs && chown harvester:harvester /app/logs
VOLUME /app/logs

USER harvester
CMD ["python", "/app/upsvar_harvester.py"]
