[loggers]
keys=root,urllib3

[handlers]
keys=logfile,stdout

[formatters]
keys=logfileformatter

[logger_root]
level=$LOG_LEVEL
handlers=logfile,stdout

[logger_urllib3]
level=$LOG_LEVEL
handlers=logfile,stdout
qualname=urllib3

[formatter_logfileformatter]
format=%(asctime)s %(name)-6s: %(levelname)s %(message)s

[handler_logfile]
class=handlers.RotatingFileHandler
level=$LOG_LEVEL
args=('$LOG_FILE','a',1000000,5)
formatter=logfileformatter

[handler_stdout]
class=StreamHandler
level=$LOG_LEVEL
formatter=logfileformatter
args=(sys.stdout,)