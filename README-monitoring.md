# About monitoring

Some additional configuration has been added to enable some metrics.

If PROM_PUSHGATEWAY_URI environment variable is set, the app will try to push some metrics on it.

## Testing the metrics

To check it works, a docker-compose file is provided with a basic prometheus/pushgateway/grafana setup.
You can start it with the app using

    docker-compose -f docker-compose.yml -f docker-compose.monitoring.yml up

You get the following accesses:
* Prometheus: http://localhost:9090/
* Grafana: http://localhost:3000/ (user _admin_, password _duh_)
* Alertmanager: http://localhost:9093/
